﻿using System.Collections.Generic;

namespace StudioKit.UserInfoService
{
	public interface IUserInfoService
	{
		/// <summary>
		/// Load user info for a list of potential identifiers, finding the first result matching the
		/// given email address.
		/// </summary>
		/// <param name="identifiers">A list of identifiers, one presumably being a PUID</param>
		/// <param name="emailAddress">An email address to match against</param>
		/// <returns>A UserInfo matching the PUID and email address or null</returns>
		UserInfo GetUser(IEnumerable<string> identifiers, string emailAddress);

		/// <summary>
		/// Load user info for the given login or PUID
		/// </summary>
		/// <param name="loginOrPuid">Either login alias or puid</param>
		/// <returns>UserInfo object or null</returns>
		UserInfo GetUser(string loginOrPuid);

		/// <summary>
		/// Load user info for the given logins or PUIDs
		/// </summary>
		/// <param name="loginsOrPuids">String array of either login aliases or identifiers</param>
		/// <param name="order">Whether to order the list by last name before returning it</param>
		/// <returns>List of UserInfo objects or empty</returns>
		List<UserInfo> GetUserList(string[] loginsOrPuids, bool order = true);
	}
}